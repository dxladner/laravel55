@if (App::environment('production'))
<nav class="navbar navbar-expand-lg navbar-custom navbar-static-top" style="background-color: #F9B608">
    <div class="container" style="text-align: center">
        <div class="col">
            HackerPair is currently a <strong>demo</strong> project. Data is regularly deleted.
            <a href="{{ route('about.index') }}">More</a>
        </div>
    </div>
</nav>
@endif
<nav class="navbar navbar-expand-lg navbar-custom navbar-static-top" style="background-color: #0D2133;">
    <div class="container">

        <a class="logo" href="{{ url('/') }}">
            HackerPair
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a href="{{ route('events.index') }}" class="nav-link">Events</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('locations.index') }}" class="nav-link">Locations</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('categories.index') }}" class="nav-link">Categories</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('about.index') }}" class="nav-link">About</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('contact.index') }}" class="nav-link">Contact Us</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('about.book') }}" class="nav-link">The Book</a>
                </li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @auth
                    <li class="nav-item dropdown">
                        <a href="#" class="dropdown-toggle nav-link" id="navbarDropdown" data-toggle="dropdown" role="button" aria-expanded="false">
                            Hi, {{ Auth::user()->first_name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" aria-labelledby="navbarDropDown">
                            <li><a href="{{ route('users.edit') }}" class="dropdown-item">Account Profile</a></li>
                            <li><a href="{{ route('favorites.edit') }}" class="dropdown-item">Favorited Events</a></li>
                            <li><a href="{{ route('users.upcoming.index') }}" class="dropdown-item">Upcoming Events</a></li>
                            <li><a href="{{ route('users.hosted.index') }}" class="dropdown-item">Hosted Events</a></li>

                            {{--  <li>{{ link_to_route('users.edit', 'Account Profile', ['id' => Auth::user()->id], ['class' => 'dropdown-item']) }}</li>
                            <li>{{ link_to_route('favorites.index', 'Favorited Events', [], ['class' => 'dropdown-item']) }}</li>
                            <li>{{ link_to_route('users.upcoming.index', 'Upcoming Events', ['user' => Auth::user()->id], ['class' => 'dropdown-item']) }}</li>
                            <li>{{ link_to_route('users.hosted.index', 'Hosted Events', ['user' => Auth::user()->id], ['class' => 'dropdown-item']) }}</li>
                              --}}
                            <li>
                                <a href="{{ route('logout') }}"
                                   class="dropdown-item"
                                   onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route('users.hosted.create') }}" class="nav-link">Post Event</a>
                    </li>
                        {{--  {!! link_to_route('users.hosted.create', 'Post Event', ['user' => Auth::user()], ['class' => 'nav-link']) !!}</li>  --}}
                @else
                    <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">Register</a></li>
                @endauth
              
            </ul>

        </div>
    </div>
</nav>