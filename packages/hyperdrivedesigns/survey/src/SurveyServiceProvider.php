<?php 

namespace Survey;

use Illuminate\Support\ServiceProvider;


class SurveyServiceProvider extends ServiceProvider
{
    public function boot() 
    {
        $this->loadRoutesFrom(__DIR__ . './routes/web.php');
        $this->loadViewsFrom(__DIR__ . './resources/views', 'survey');
    }

    public function register()
    {

    }
}
