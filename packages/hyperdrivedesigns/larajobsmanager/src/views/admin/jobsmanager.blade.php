<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Jobs</div>

            <div class="panel-body">
              <ul>
                @foreach ($larajobs as $larajob)
                    <li>{{ $larajob->title}}</li>
                @endforeach
              </ul>
            </div>
        </div>
    </div>
</div>