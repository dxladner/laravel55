<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');
/* NAMED ROUTES */
//Route::get('events', 'EventsController@index');
//Route::get('events/{id}', 'EventsController@show')->name('events.show');
//Route::get('events/category/{category}/{subcategory?}', 'EventsController@category');


Route::get('/home', 'HomeController@index')->name('home');

/* ABOUT */
Route::view('about', 'about.index')->name('about.index');
Route::view('about/book', 'about.book')->name('about.book');
Route::view('about/faq', 'about.faq')->name('about.faq');
Route::view('about/privacy', 'about.privacy')->name('about.privacy');
Route::view('about/tos', 'about.tos')->name('about.tos');

/* CONTACT */
Route::view('contact', 'contact.index')->name('contact.index');
Route::view('contact/create', 'contact.create')->name('contact.create');

/* EVENTS */
Route::get('events', 'EventsController@index')->name('events.index');
Route::get('events/{id}', 'EventsController@show')->name('events.show');

/* LANGUAGES */
Route::get('languages', 'LanguagesController@index')->name('languages.index');

/* LOCATIONS */
Route::get('locations', 'LocationsController@index')->name('locations.index');

/* MAPS */
Route::get('map', 'MapsController@index')->name('maps.index');

Route::get('categories', 'LocationsController@index')->name('categories.index');


Auth::routes();

Route::get('/admin', 'AdminController@index')->name('admin');

/*==================== Implementing Laravel ====================*/
Route::get('/container', function()
{
    // get Application instance
    $app = App::getFacadeRoot();
    $app['some_array'] = array('foo' => 'bar');

    var_dump($app['some_array']);
});

Route::get('/container2', function()
{
    // get Application instance
    $app = App::getFacadeRoot();
    $app['say_hi'] = function()
    {
        return "HEY NOW!";
    };

    return $app['say_hi'];
});

interface GreetableInterface {
    public function greet();
}

class HelloWorld implements GreetableInterface {
    public function greet()
    {
        return "HEY NOW!";
    }
}

Route::get('/containerhey', function()
{
    // get Application instance 
    $app = App::getFacadeRoot();

    $app->bind('GreetableInterface', function()
    {
        return new HelloWorld;
    });

    $greeter = $app->make('GreetableInterface');

    return $greeter->greet();
});

Route::get('/containercontroller', 'ContainerController@container');