@extends('layouts.master')

@section('content')
      <h1>Welcome to HackerPair</h1>
      <p>
        <a href="{{ route('events.show', ['id' => 42]) }}">Laravel Hacking and Coffee
        </a>
      </p>
       ID: {{ $id }} and title is: {{ $name }}
       <p>
         My username is {{ $username or 'DL'}} and my dog is named {{ $dogname }}!
         My address is: <br>
         {!! $address !!} {!! $city !!}, {!! $state !!} {!! $zip !!}
       </p>
       <h5>Cities</h5>
       <ul>
        @forelse ($cities as $city)
          <li> {{ $city }}</li>
        @empty
          <li>NO CITIES LISTED!</li>
        @endforelse
       </ul>

@endsection

@section('ads')
<hr>
 <h5>Sports</h5>
 <ul>
   @foreach ($teams as $team)
   <li>
     {{ $team }}
     @if(strpos($team, 'Laravel') != false)
      (sweet framework!)
    @endif
  </li>
  @endforeach
 </ul>
@endsection

<h5>ADS</h5>
<table>
  @foreach($teams as $team)
   @include('partials._row', ['team' => $team])
 @endforeach
</table>
