<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware([ 'admin']);
    }
    public function index()
    {
        dd(Auth::user());
        return view('admin.index');
    }
}
