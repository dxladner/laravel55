<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use HelloWorld;

class ContainerController extends Controller
{
    protected $greeter;

    // Class dependency:: HelloWorld
    public function __construct(HelloWorld $greeter)
    {
        $this->greeter = $greeter;
    }

    public function container()
    {
        return $this->greeter->greet();
    }
}
