<?php

use Illuminate\Database\Seeder;
use App\Event;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->truncate();
        $faker = \Faker\Factory::create();

        foreach(range(1,10) as $index)
        {
            Event::create([
                'name' => $faker->sentence(2),
                'enabled' => 1,
                'city' => $faker->city,
                'venue' => $faker->company,
                'description' => $faker->paragraphs(1, true)
            ]);
        }
        // $this->call(UsersTableSeeder::class);
    }
}
