<?php

namespace Hyperdrivedesigns\Larajobsmanager;

use Illuminate\Support\ServiceProvider;

class LaraJobsManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('Hyperdrivedesigns\Larajobsmanager\LarajobsmanagerController');
    }
}
