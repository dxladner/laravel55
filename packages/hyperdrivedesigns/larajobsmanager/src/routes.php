<?php 
Route::group(['middleware' => ['web', 'auth']], function(){
    Route::get('/jobs', 'hyperdrivedesigns\larajobsmanager\LarajobsmanagerController@index')->name('jobs');
});