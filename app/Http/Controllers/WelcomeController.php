<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    function index()
    {
      return view('welcome.index');
    }

    public function show($id)
    {
      $username = 'dxladner';
      $dogname = 'Gabbie';

      $data = [
        'address' => '123 Oak St',
        'city'    => 'Dallas',
        'state'   => 'Texas',
        'zip'     => '75028'
      ];

      $cities = [
        'Boston',
        'New York',
        'New Orleans',
        'Dallas'
      ];

      $teams = [
        'MLB',
        'NHL',
        'NFL',
        'NBA'
      ];

      return view('welcome.index', compact('username', 'dogname'))
        ->with('id', $id)
        ->with('name', 'Laravel Hacking and Coffee')
        ->with($data)
        ->with('cities', $cities)
        ->with('teams', $teams);
    }

    public function category($category, $subcategory = '')
    {
      dd("Category: {$category} Subcategory: {$subcategory}");
    }
}
