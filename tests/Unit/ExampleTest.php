<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }

    public function testSomeValueIsFalse()
    {
      $this->assertFalse(false);
    }

    public function testUserFullNameIsDarrenLadner()
    {
      $fullName = "Darren Ladner";
      $this->assertEquals("Darren Ladner", $fullName);
    }

    public function testUserHasThreeBikes()
    {
      $bikes = ['Felt', 'Cervelo', 'Argon', 'Giant'];
      $this->assertCount(3, $bikes);
    }
}
