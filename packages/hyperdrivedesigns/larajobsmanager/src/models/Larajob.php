<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Larajob extends Model
{
  protected $fillable = ['title', 'description', 'city', 'state', 'job_start_range', 'job_status', 'listing_state_date', 'listing_end_date'];

}