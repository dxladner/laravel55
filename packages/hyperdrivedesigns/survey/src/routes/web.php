<?php 

$namespace = 'Survey\Http\Controllers';

Route::group([
    'namespace' => $namespace,
    'prefix'    => 'audit',
],
    function() {
    
        Route::get('/survey',  'SurveyController@index');

    Route::group(['middleware' => ['web', 'auth']], function(){
        Route::get('/test', 'SurveyController@test');
    });

});