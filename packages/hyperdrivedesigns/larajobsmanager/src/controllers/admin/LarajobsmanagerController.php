<?php 

namespace Hyperdrivedesigns\Larajobsmanager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class LarajobsmanagerController extends Controller 
{
    public function __construct(Request $request)
    {
        $this->middleware(['admin']);
    }

    public function index()
    {
        echo Carbon::now()->toDateTimeString();
    }
}
